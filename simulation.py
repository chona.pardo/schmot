from methods import *
from pprint import pprint
from assistant import *
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


text = input()
#print(analize(text))
translate = translate(text, "es-en")
translate_text = translate["translations"][0]["translation"]
pprint(analizeWithWatson(translate_text))