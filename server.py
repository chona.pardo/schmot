from flask import Flask, request
from twilio import twiml
from twilio.rest import Client
from schmot import *
import ssl
from methods import *

https_context = ssl._create_unverified_context


app = Flask(__name__)

@app.route('/', methods=['POST'])
def main():

	user_num = request.form['From']

	message_body = request.form['Body']

	response = analize(message_body)

	sendToNum(response, user_num)

	return response


if __name__ == '__main__':
    app.run()