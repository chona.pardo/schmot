from weather import *
from mail import *
from assistant import *
from memes import *

def analize(text):
	firstWord = text.split()[0]
	res = ""
	# if text == 'hola':
	# 	return 'hola'
	if firstWord == "Temperatura" or firstWord == "temperatura":
		before_keyword, keyword, after_keyword = text.partition("en")
		res = cityWeather(after_keyword)

	if firstWord == "mail" or firstWord == "email":
		before_keyword, keyword, after_keyword = text.partition(" a ")
		res = sendEmail(after_keyword)

	if firstWord == "meme":
		res = make_a_meme()

	return res

methods = {
	'weatherMethod': cityWeather,
	'sendEmailMethod': sendEmail
}

def analizeWithWatson(text):
	response = sendToAssistant(text)
	response_all = json.dumps(response, indent=2)
	response_answer = response['output']['generic'][0]['text']
	response_entities = response['output']['entities'][0]
	if (response_answer == "weatherMethod"):
		return methods[response_answer](response_entities["value"])

	if (response_answer[0:15] == "sendEmailMethod"):
		before_keyword, keyword, after_keyword = response_answer.partition("\n")
		return sendEmail(after_keyword)
	

	#print(response_all)
	#print(response_answer)
	#print(response_entity)
