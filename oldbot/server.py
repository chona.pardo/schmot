from flask import Flask, request
from twilio import twiml
import os
from time import sleep
from twilio.rest import Client 
#from pynput.mouse import Button, Controller

server = 'https://breezy-cat-37.localtunnel.me'
account_sid = 'AC5a91b40dcbc7379b67771e52b8b8f1ec' 
auth_token = '86bcbea73d539e3b083f04b5f869abc5' 
client = Client(account_sid, auth_token)

def replyText(text):
	message = client.messages.create( 
		                              from_='whatsapp:+14155238886',  
		                              body=text,
		                              to='whatsapp:+5491154026445'
		                          )
	print(message.sid)
def replyImage(media):
	message = client.messages.create( 
		                              from_='whatsapp:+14155238886',
		                              media_url=media,
		                              to='whatsapp:+5491154026445'
		                          )
	print(message.sid)


def send(text, media = None):
	if media is not None:
		message = client.messages.create( 
		                              from_='whatsapp:+14155238886',  
		                              body=text,
		                              media_url=media,
		                              to='whatsapp:+5491154026445'
		                          )
	else:
		message = client.messages.create( 
		                              from_='whatsapp:+14155238886',  
		                              body=text,
		                              to='whatsapp:+5491154026445'
		                          ) 
 
	print(message.sid)

def googleSearch(text):
	
	url = "https://www.google.com/search?q={}".format(text.replace(' ', '+'))
	#os.system('open -a "Google Chrome" batman.mp4')
	os.system('open -a "Google Chrome" ' + url)

	sleep(3)
	os.system("screencapture static/screen.png")
	sleep(3)

	return ['Google Search for: ' + text, server+'/static/screen.png']

def moveMouse(coordinates):
	#x, y = coordinates.split(',')
	#x = int(x)
	#y = int(y)
	#mouse = Controller()
	#mouse.position = (x, y)
	#sleep(3)
	#os.system("screencapture static/screen.png")
	#sleep(3)

	return ['Mouse moved to ' + coordinates, server+'/static/screen.png']


def clickMouse():
	#mouse = Controller()
	#mouse.press(Button.left)
	#mouse.release(Button.left)
	#sleep(3)
	#os.system("screencapture static/screen.png")
	#sleep(3)

	return ['Mouse clicked', server+'/static/screen.png']


def scrollMouse(x, y):
	#x, y = coordinates.split(',')
	#x = int(x)
	#y = int(y)
	#mouse = Controller()
	#mouse.scroll(x, y)
	#sleep(3)
	#os.system("screencapture static/screen.png")
	#sleep(3)

	return ['Mouse scrolled', server+'/static/screen.png']



def screenshot():
	sleep(3)
	os.system("screencapture static/screen.png")
	sleep(3)

	return ['Screenshot', server+'/static/screen.png']

def terminal(param):
	os.system(param)
	sleep(3)
	os.system("screencapture static/screen.png")
	sleep(3)
	return ['Listo', server+'/static/screen.png']

def AppleScript(param):
	os.system('osascript -e ' + param)
	sleep(3)
	os.system("screencapture static/screen.png")
	sleep(3)
	return ['Listo', server+'/static/screen.png']

def helper():
	helpText = ', '.join(list(commands.keys())	)
	print(helpText)
	sleep(3)
	os.system("screencapture static/screen.png")
	sleep(3)
	return [helpText, server+'/static/screen.png']

def kill():
	quit()
	return None

commands = {'google': googleSearch,
			'xy': moveMouse,
			'click': clickMouse,
			'scroll': scrollMouse,
			'terminal':terminal,
			'AppleScript':AppleScript,
			'help': helper,
			'screenshot': screenshot,
			'kill': kill}

tryAgainMessage = 'Try one of this commands: ' + ', '.join(list(commands.keys()))

def analize(text):

	try:
		cmmd, param = text.split(':')
		param = param
		print(cmmd, param)
		if cmmd in commands:
			return commands[cmmd](param)
		else:
			return tryAgainMessage
	except:
		cmmd = text
		if cmmd in commands:
			return commands[cmmd]()
		else:
			return tryAgainMessage



app = Flask(__name__)

@app.route('/', methods=['POST'])
def sms():

	message_body = request.form['Body']

	response = analize(message_body)

	if response is str:
		send(response)
	else:
		send(response[0], response[1])
	return str(response)

if __name__ == '__main__':
    app.run()