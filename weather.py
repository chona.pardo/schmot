import requests, json 

def cityWeather(cityName):
	api_key = "0b8457b5c2cef626d1fc0b5a1294d49a"
	base_url = "http://api.openweathermap.org/data/2.5/weather?q="
	complete_url = base_url + cityName + "&appid=" + api_key 
	response = requests.get(complete_url) 
	res = response.json() 
	if res["cod"] != "404": 
	    y = res["main"] 
	    current_temperature = y["temp"] #Temperatura en Kelvin
	    current_temperature_in_celsius = round(current_temperature - 273.15)
	    current_pressure = y["pressure"] # presion en hPa
	    current_humidiy = y["humidity"] 
	    z = res["weather"] 
	    weather_description = z[0]["description"] 
	    return "La temperatura en " + cityName + " es " + str(current_temperature_in_celsius) + "°C"

	else: 
	    return " City Not Found "