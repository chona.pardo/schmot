from watson_developer_cloud import AssistantV2
import os
from os.path import join, dirname
import json
from pprint import pprint
from watson_developer_cloud.websocket import RecognizeCallback, AudioSource
import ssl

from ibm_watson import AssistantV2, LanguageTranslatorV3
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from langdetect import detect

https_context = ssl._create_unverified_context

def sendToAssistant(textInput):

	
	#Hay que cambiar estas keys por las de una nueva cuenta de ibm
	assistant_apiKey = 'swTPc3miYjBzHV1t2J_hka1mPVYqYqZE60smZLsNq7L7'
	assistant_url = 'https://api.eu-gb.assistant.watson.cloud.ibm.com/instances/53d4c6b2-c14b-4655-a5eb-0a109d4f5f89/'
	assistant_id = '4cfcf9d8-6cdb-4905-9506-51ca262ab9ba'



	authenticator = IAMAuthenticator(assistant_apiKey)
	assistant = AssistantV2(
	    version='2020-02-05',
	    authenticator=authenticator
	)


	assistant.set_service_url(assistant_url)

	assistant.set_disable_ssl_verification(True)

	session_id = assistant.create_session(
	    assistant_id=assistant_id
	).get_result()['session_id']


	response = assistant.message(
	    assistant_id=assistant_id,
	    session_id=session_id,
	    input={
	        'message_type': 'text',
	        'text': textInput
	    }
	).get_result()

	return response
	#print(json.dumps(response, indent=2)['output']['generic']['text'])


def translate(textInput, lang='en-es'):
	#Hay que cambiar estas keys por las de una nueva cuenta de ibm
	language_translator_apiKey = 'j2CAedmab9okcB6oaLJKU7Le4QgYdAG_PDkXtOdfSS40'
	language_translator_url = 'https://api.eu-gb.language-translator.watson.cloud.ibm.com/instances/ebbee957-274e-4058-9b50-78e769c35828'

	authenticator = IAMAuthenticator(language_translator_apiKey)
	language_translator = LanguageTranslatorV3(
	    version='2018-05-01',
	    authenticator=authenticator
	)

	language_translator.set_service_url(language_translator_url)
	language_translator.set_disable_ssl_verification(True)

	translation = language_translator.translate(
	    text=textInput,
	    model_id=lang).get_result()

	return translation


"""while True:

	inputText = input('Input: ')
	lang = detect(inputText)
	print('Language: '+lang)
	to_send = translate(inputText, lang+'-en')['translations'][0]['translation']
	print('Translation to send: '+to_send)
	response = sendToAssistant(to_send)
	pprint('Detected intent: '+response['output']['intents'][0]['intent'])
	pprint('Response: '+response['output']['generic'][0]['text'])"""

